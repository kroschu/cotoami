#!/bin/bash

#
# Run backend services as docker containers
#

if [ -n "$DOCKER_HOST" ]; then
  DOCKER_HOST_IP=$(echo $DOCKER_HOST | sed 's/^.*\/\/\(.*\):[0-9][0-9]*$/\1/g')
else
  DOCKER_HOST_IP="treba.tk"
fi

# Redis
echo
echo "# Running redis..."
docker run  -dit --restart unless-stopped --name redis  -p 6379:6379 arm32v7/redis
export COTOAMI_REDIS_HOST=$DOCKER_HOST_IP

# PostgreSQL
echo
echo "# Running postgres..."
docker run  -dit --restart unless-stopped --name postgres  -p 5435:5432 -e POSTGRES_USER=postgres  -e POSTGRES_PASSWORD=postgres -e POSTGRES_DB=cotoami_dev arm32v7/postgres
export COTOAMI_DEV_REPO_HOST=$DOCKER_HOST_IP

# Neo4j
echo
echo "# Running neo4j..."
docker run  -dit --restart unless-stopped --name neo4j -p 7687:7687 -p 7474:7474 -e NEO4J_AUTH=none --volume=$HOME/neo4j/data:/data joov/rpi-neo4j
export COTOAMI_NEO4J_HOST=$DOCKER_HOST_IP

# Mail server
echo
echo "# Running maildev..."
docker run  -dit --restart unless-stopped --name maildev -p 8080:80 -p 25:25 kroschu/armhf-maildev bin/maildev --web 80 --smtp 25 --outgoing-host smtp.gmail.com  --outgoing-secure   --outgoing-user 'tukroschu@gmail.com'  --outgoing-pass 'agni0.523' --auto-relay kroschu@mail.ua
export COTOAMI_SMTP_SERVER=$DOCKER_HOST_IP
export COTOAMI_SMTP_PORT=25
echo
echo "You can check sign-up/in mails at http://$DOCKER_HOST_IP:8080"

# Mail sender
export COTOAMI_EMAIL_FROM="no-reply@cotoa.me"
